<?php

use Illuminate\Database\Seeder;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('barang')->insert([
            'barcode' => 'MK001',
            'nama' => 'Soklin',
            'harga_beli' => '2000',
            'harga_jual' => '4000',
            'stock' => '20',
        ]);
        DB::table('barang')->insert([
            'barcode' => 'MK002',
            'nama' => 'sabun',
            'harga_beli' => '2000',
            'harga_jual' => '4000',
            'stock' => '20',
        ]);
        DB::table('barang')->insert([
            'barcode' => 'MK003',
            'nama' => 'tisu',
            'harga_beli' => '2000',
            'harga_jual' => '4000',
            'stock' => '20',
        ]);
        DB::table('barang')->insert([
            'barcode' => 'MK004',
            'nama' => 'tisu',
            'harga_beli' => '2000',
            'harga_jual' => '4000',
            'stock' => '20',
        ]);
        DB::table('barang')->insert([
            'barcode' => 'MK005',
            'nama' => 'tisu',
            'harga_beli' => '2000',
            'harga_jual' => '4000',
            'stock' => '20',
        ]);
    }
}
