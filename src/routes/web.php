<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('barang', 'BarangController@index')->name('barang-views');
Route::get('barang/create', 'BarangController@create')->name('create-barang');
Route::post('barang/store', 'BarangController@store');
Route::get('barang/{id}', 'BarangController@update');
Route::get('delete/{id}', 'BarangController@delete')->name('delete-barang');
Route::get('update/{id}', 'BarangController@update')->name('update-barang');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
