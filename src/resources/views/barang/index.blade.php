@extends('layouts.app') @section('content')
<div class="container" style="margin-top: 50px;">
    @if ($message = Session::get('success'))
    <div class="card-panel">
        <span class="blue-text text-darken-2">{{$message}}</span>
    </div>

    @endif

    <table class="stripped responsive-table highlight">
        <a href="{{ route( 'create-barang') }} " class="waves-effect waves-light btn ">Tambah Data Barang</a>
        <thead>
            <tr>
                <th>Barcode</th>
                <th>Nama Barang</th>
                <th>Harga Beli</th>
                <th>Harga Jual</th>
                <th>Stock Barang</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($barang as $item)
            <tr>
                <td>{{$item->barcode}}</td>
                <td>{{$item->nama}}</td>
                <td>@convert($item->harga_beli)</td>
                <td>@convert($item->harga_jual)</td>
                <td>{{$item->stock}}</td>
                <td>
                    <a class="waves-effect waves-light btn-small" href="{{route( 'delete-barang', [ 'id'=> $item->id] )}}">update</a>
                    <a class="waves-effect waves-danger btn-small" href="{{route('delete-barang', ['id' => $item->id] )}}" onclick="return confirm('Are you sure?')">delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection