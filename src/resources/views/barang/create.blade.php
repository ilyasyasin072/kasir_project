@extends('layouts.app') @section('content')
<form action="/barang/store" method="POST">
    @csrf
    <div class="container" style="margin-top: 10px;">
        <div class="row">
            <div class="col-md-12">
                <!-- 
                <div class="form-group">
                    {!! Form::label('barcode', 'Barcode:', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        <input type="text" class="form-control" placeholder="Barcode Code" name="barcode">
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('nama', 'Barcode:', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        <input type="text" class="form-control" placeholder="Nama Barang" name="nama" id="nama">
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('harg_beli', 'Barcode:', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        <input type="number" class="form-control" placeholder="Harga Beli" name="harga_beli" id="nama">
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('textarea', 'Textarea', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        <input type="number" class="form-control" placeholder="Harga Jual" name="harga_jual" id="nama">
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('select', 'Select w/Default', ['class' => 'col-lg-2 control-label'] ) !!}
                    <div class="col-lg-10">
                        <input type="number" class="form-control" placeholder="Harga Jual" name="stock" id="nama">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-right'] ) !!}
                    </div>
                </div> -->

                <div class="row">
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="barcode" type="text" class="validate" name="barcode" required>
                                <label for="barcode">Barcode</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="nama" type="text" class="validate" name="nama" required>
                                <label for="nama">Nama Barang</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="harga_beli" type="text" name="harga_beli" class="validate" required>
                                <label for="harga_beli">Harga Beli</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="harga_jual" type="text" name="harga_jual" class="validate" required>
                                <label for="harga_jual">Harga Beli</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="stock" type="number" class="validate" name="stock" required>
                                <label for="stock">Stock</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <div class="col-lg-10 col-lg-offset-2">
                                    {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-right'] ) !!}
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</form>
@endsection