@extends('layouts.app') @section('content')

<!-- {!! Form::open(['url' => '/barang', 'class' => 'form-horizontal']) !!} -->
@foreact($barang as $p)
<form action="/barang/update" method="POST">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="form-group">
                    {!! Form::label('barcode', 'Barcode:', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        <!-- {!! Form::text('barcode', $value = null, ['class' => 'form-control', 'placeholder' => 'Barcode']) !!} -->
                        <input type="text" class="form-control" placeholder="Barcode Code" value={} name="barcode">
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('nama', 'Barcode:', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        <input type="text" class="form-control" placeholder="Nama Barang" name="nama" id="nama">
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('harg_beli', 'Barcode:', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        <!-- {!! Form::text('harga_jual', $value = null, ['class' => 'form-control', 'placeholder' => 'Harga Beli']) !!} -->
                        <input type="number" class="form-control" placeholder="Harga Beli" name="harga_beli" id="nama">
                    </div>
                </div>

                <!-- Text Area -->
                <div class="form-group">
                    {!! Form::label('textarea', 'Textarea', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        <!-- {!! Form::textarea('textarea', $value = null, ['class' => 'form-control', 'rows' => 3]) !!} -->
                        <!-- <span class="help-block">A longer block of help text that breaks onto a new line and may extend beyond one line.</span> -->
                        <input type="number" class="form-control" placeholder="Harga Jual" name="harga_jual" id="nama">
                    </div>
                </div>


                <!-- Select With One Default -->
                <div class="form-group">
                    {!! Form::label('select', 'Select w/Default', ['class' => 'col-lg-2 control-label'] ) !!}
                    <div class="col-lg-10">
                        <!-- {!! Form::select('select', ['S' => 'Small', 'L' => 'Large', 'XL' => 'Extra Large', '2XL' => '2X Large'], 'S', ['class' => 'form-control' ]) !!} -->
                        <input type="number" class="form-control" placeholder="Harga Jual" name="stock" id="nama">
                    </div>
                </div>

                <!-- Submit Button -->
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-right'] ) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endforeach
<!-- {!! Form::close() !!}  -->
@endsection