<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Barang;
use Validator;
class BarangController extends Controller
{
    public function index()
    {
        $barang = Barang::all();
        return view('barang.index', ['barang' => $barang]);
    }

    public function create()
    {
        return view('barang.create');
    }

    public function store(Request $request)
    {   
        // $validator = Validator::make(request()->all(), [
        //     'barcode'       => 'required|max:30',
        //     'name'          => 'required',
        //     'harga_beli'    => 'required',
        //     'harga_jual'    => 'required',
        // ]);

        // if($validator->fails())
        // {
        //     redirect()
        //         ->back()
        //         ->withErrors($validator->errors());
        // }

        $barcode_type       = $request->barcode;
        $nama               = $request->nama;
        $harga_beli         = $request->harga_beli;
        $harga_jual         = $request->harga_jual;
        $stock = $request->stock;

        $data = new Barang();

        $data->barcode = $barcode_type;
        $data->nama = $nama;
        $data->harga_beli = $harga_beli;
        $data->harga_jual = $harga_jual;
        $data->stock = $stock;

        $data->save();

        return redirect('barang')->with(['success' => 'Data Barang Berhasil Disimpan']);;

    }

    public function edit($id)
    {
        $barang = Barang::find($id);
        return view('index.edit', ['barang' => $barang]);
    }

    public function update(Request $request, $id)
    {
        $data = Barang::find($id);

        $barcode_type   = $request->barcode;
        $nama           = $request->nama;
        $harga_beli     = $request->harga_beli;
        $harga_jual     = $request->harga_jual;
        $stock          = $request->stock;

        $data->barcode      = $barcode_type;
        $data->nama         = $nama;
        $data->harga_beli   = $harga_beli;
        $data->harga_jual   = $harga_jual;
        $data->stock        = $stock;

        $data->save();
        return redirect('barang')->with(['success' => 'Pesan Berhasil update']);
    }

    public function delete($id)
    {
        $data = Barang::find($id);
        $data->delete();

        return redirect('barang')->with(['success' => 'Pesan Berhasil Dihapus']);
    }
}
